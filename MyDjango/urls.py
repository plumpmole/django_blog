
from django.contrib import admin
from django.urls import path, re_path
# 导入项目应用index
from index.views import index
# 配置媒体文件夹media
from django.views.static import serve
from django.conf import settings


urlpatterns = [
    path('admin/', admin.site.urls), # 映射管理后台
    path('', index),    # 映射index应用
   # 配置媒体文件的路由地址
    # re_path('media/(?P<path>.*)',serve,
    #    {'document_root': settings.MEDIA_ROOT}, name='media')
    path('media/<path:path>', serve, {'document_root': settings.MEDIA_ROOT}, name='media')
]